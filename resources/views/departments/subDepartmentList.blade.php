@foreach($subdepartments as $subdepartment)
 <ul class="pl-5">
    <li >

        <div class="grid grid-cols-8 gap-1">
            <div class="col-span-6 ">
                <a href="{{ url('dashboard/' . $subdepartment->name) }}">
                <button class="bg-green-400 text-white px-4 py-3 
                rounded font-medium w-full">{{$subdepartment->name}}</button>
                </a>
            </div>
            <div class="col-span-1">
                <a href="{{ url('edit_department/' .$subdepartment->id) }}">
                    <button class="bg-gray-400 text-white px-4 py-3 rounded font-medium w-full">Edit</button>
                </a>
            </div>
            <div class="col-span-1">
            <form action="{{ route('departments.destroy', $subdepartment) }}" method="post"> 
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="bg-red-500 text-white px-4 py-3 rounded font-medium w-full">Delete</button>
                </form>
            </div>
        </div>

        <br></br>
    </li> 
    
    @if(count($subdepartment->subdepartment))
        @include('departments.subDepartmentList',
        ['subdepartments' => $subdepartment->subdepartment])
    @endif
 </ul> 
@endforeach