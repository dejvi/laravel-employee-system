@extends('layouts.app')

@section('content')
@auth
@if (auth()->user()->is_admin)
    <div class="flex justify-center">
        <div class="w-4/12 bg-white p-6 rounded-lg">
            <form action="{{ route('departments.update', $data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-4">
                    <label for="name" class="sr-only">Department Name</label>
                    <input type="text" name="name" id="name" placeholder="Department name" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') 
                    border-red-500 @enderror" value="{{ $data->name }}">

                    @error('name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label for="parentDepartment" class="sr-only">The parent department</label>
                    <select name="parentDepartment" id="parentDepartment" class="bg-gray-100 border-2 w-full p-4 rounded-lg">
                        <option value="0">Has no parent</option>
                    @foreach($departments as $department)
                        <option value="{{ $department->id }}">{{$department->name}}</option>
                    @endforeach
                    </select>
                </div>

                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full">Update Department</button>
                </div>
            </form>
        </div>
    </div>

@else
<div class="flex justify-center">
  <div class="w-4/12 bg-white p-6 rounded-lg">
    <p class="text-red-500">Only admins can view this information!</p>
  </div>
</div>
@endif
@endauth

@guest
<div class="flex justify-center">
  <div class="w-4/12 bg-white p-6 rounded-lg">
    <p class="text-red-500">You need to be logged in to view this information.</p>
  </div>
</div>
@endguest
@endsection