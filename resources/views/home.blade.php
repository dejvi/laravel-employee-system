@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-5/12 bg-white p-6 rounded-lg">
        @auth
            <div>

            @foreach($parentDepartments as $department)
            <div class="grid grid-cols-8 gap-1">
            <div class="col-span-6">
                <a href="{{ url('dashboard/' . $department->name) }}">
                    <button class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full">{{$department->name}}</button>
                </a>
            </div>
            <div class="col-span-1">
                <a href="{{ url('edit_department/' .$department->id) }}">
                    <button class="bg-gray-400 text-white px-4 py-3 rounded font-medium w-full">Edit</button>
                </a>
            </div>
            <div class="col-span-1">

                <form action="{{ route('departments.destroy', $department) }}" method="post"> 
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="bg-red-500 text-white px-4 py-3 rounded font-medium w-full">Delete</button>
                </form>

            </div>
            </div>
            <br></br>
            @if(count($department->subdepartment))
                @include('departments.subDepartmentList',['subdepartments' => $department->subdepartment])
            @endif
            @endforeach
                <br></br>
                <a href="{{ route('addDepartment') }}">
                    <button class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full">Add a new department</button>
                </a>
            </div>
            @endauth
            @guest
            <p class="text-red-500">You need to be logged in to view this information.</p>
            @endguest
        </div>
    </div>
@endsection