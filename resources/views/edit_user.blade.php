@extends('layouts.app')

@section('content')

@auth
@if (auth()->user()->is_admin or auth()->user()->id == $data->id)
    <div class="flex justify-center">
        <div class="w-4/12 bg-white p-6 rounded-lg">
            <form action="{{ route('users.update', $data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="mb-4">
                        @if ($data->image)
                        <div class="flex flex-wrap justify-center">
                            <div class="w-6/12 sm:w-4/12 px-4">
                                <img class="shadow rounded-full max-w-full h-auto align-middle border-none" src="{{ asset('storage/' . $data->image) }}" alt="">
                            </div>
                        </div>
                        @else
                        <div class="flex flex-wrap justify-center">
                            <div class="w-6/12 sm:w-4/12 px-4">
                                <img class="h-30 w-30 rounded-full" src="{{ asset('storage/uploads/' . 'default.jpg') }}" alt="">
                            </div>
                        </div>
                        @endif
                        <br>
                    <label for="name" class="sr-only">Name</label>
                    <input type="text" name="name" id="name" placeholder="Your name" class="bg-gray-100 border-2 w-full p-4 rounded-lg 
                    @error('name') border-red-500 @enderror" value="{{ $data->name }}">

                    @error('name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label for="username" class="sr-only">Username</label>
                    <input type="text" name="username" id="username" placeholder="Username" class="bg-gray-100 border-2 w-full p-4 rounded-lg
                    @error('username') border-red-500 @enderror" value="{{ $data->username }}">

                    @error('username')
                        <div class="text-red-500 mt-2 text-sm">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label for="email" class="sr-only">Email</label>
                    <input type="text" name="email" id="email" placeholder="Your email" class="bg-gray-100 border-2 w-full p-4 rounded-lg
                    @error('email') border-red-500 @enderror" value="{{ $data->email }}">

                    @error('email')
                        <div class="text-red-500 mt-2 text-sm">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label for="department" class="sr-only">Choose a department</label>
                    <select name="department" id="department" class="bg-gray-100 border-2 w-full p-4 rounded-lg">

                    @foreach($departments as $department)
                        <option value="{{ $department->name }}">{{$department->name}}</option>
                    @endforeach
                    </select>
                </div>

                <div class="mb-4">
                    <label for="title" class="sr-only">Title</label>
                    <input type="text" name="title" id="title" placeholder="Your title" class="bg-gray-100 border-2 w-full p-4 rounded-lg 
                    @error('title') border-red-500 @enderror" value="{{ $data->title }}">

                    @error('title')
                        <div class="text-red-500 mt-2 text-sm">
                            {{$message}}
                        </div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="image">Profile Image</label>
                    <input type="file" name="image">
                </div>

                <div>
                    <button type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium w-full">Update</button>
                </div>
            </form>
        </div>
    </div>
@else
<div class="flex justify-center">
  <div class="w-4/12 bg-white p-6 rounded-lg">
    <p class="text-red-500">Only admins can view this information!</p>
  </div>
</div>
@endif
@endauth

@guest
<div class="flex justify-center">
  <div class="w-4/12 bg-white p-6 rounded-lg">
    <p class="text-red-500">You need to be logged in to view this information.</p>
  </div>
</div>
@endguest
@endsection