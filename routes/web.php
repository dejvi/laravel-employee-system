<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostLikeController;
use App\Http\Controllers\UserPostController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\DepartmentController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [DepartmentController::class, 'index'])->name('home');

Route::get('/add_department', [DepartmentController::class, 'add'])->name('addDepartment');
Route::post('/add_department', [DepartmentController::class, 'store']);
Route::delete('/delete_department/{department}', [DepartmentController::class, 'destroy'])->name('departments.destroy');
Route::get('/edit_department/{id}', [DepartmentController::class, 'edit']);
Route::post('/edit_department/{id}', [DepartmentController::class, 'update'])->name('departments.update');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/dashboard/{department}', [DashboardController::class, 'showDepartment'])->name('department');
Route::delete('/users/{user}', [DashboardController::class, 'destroy'])->name('users.destroy');
Route::get('/users/edit/{id}', [DashboardController::class, 'edit']);
Route::post('/users/edit/{id}', [DashboardController::class, 'update'])->name('users.update');


Route::get('/users/{user:username}/posts', [UserPostController::class, 'index'])->name('users.posts');

Route::post('/logout', [LogoutController::class, 'store'])->name('logout');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'store']);

Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/posts', [PostController::class, 'index'])->name('posts');
Route::get('/posts/{post}', [PostController::class, 'show'])->name('posts.show');
Route::post('/posts', [PostController::class, 'store']);
Route::delete('/posts/{post}', [PostController::class, 'destroy'])->name('posts.destroy');


Route::post('/posts/{post}/likes', [PostLikeController::class, 'store'])->name('posts.likes');
Route::delete('/posts/{post}/likes', [PostLikeController::class, 'destroy'])->name('posts.likes');

Route::get('/chat', [ChatController::class, 'index'])->name('chat');

Route::get('/contacts', [ContactsController::class, 'get']);
Route::get('/conversation/{id}', [ContactsController::class, 'getMessagesFor']);
Route::post('/conversation/send', [ContactsController::class, 'send']);
