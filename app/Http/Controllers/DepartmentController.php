<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;

class DepartmentController extends Controller
{

    public function __contruct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $parentDepartments = Department::where('parent_id', 0)->get();

        return view('home', compact('parentDepartments'));
    }

    public function add()
    {
        $departments = Department::all();

        return view('departments.addDepartment', [
            'departments' => $departments,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        Department::create([
            'name' => $request->name,
            'parent_id' => $request->parentDepartment,
        ]);

        return redirect()->route('home');
    }

    public function destroy(Department $department)
    {
        $department->delete();

        return back();
    }

    public function edit($id)
    {
        $data = Department::find($id);
        $departments = Department::all();

        return view('departments.edit_department', [
            'data' => $data,
            'departments' => $departments
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        Department::find($id)->update([
            'name' => $request->name,
            'parent_id' => $request->parentDepartment
        ]);

        return redirect()->route('home');
    }
}
