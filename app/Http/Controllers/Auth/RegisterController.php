<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index()
    {

        $departments = Department::all();

        return view('auth.register', [
            'departments' => $departments,
        ]);
    }

    public function store(Request $request)
    {
        //validation image could be optional

        /*
        $this->tap(request()->validate([
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed',
            'title' => 'required|max:255',
        ]), function () {
                if (request()->hasFile('image')) {
                    request()->validate([
                        'image'=> 'file|image|max:5000'
                    ]);
                }
        });
        */

        if (request()->has('image')) {
            $this->validate($request, [
                'name' => 'required|max:255',
                'username' => 'required|max:255',
                'email' => 'required|email|max:255',
                'password' => 'required|confirmed',
                'title' => 'required|max:255',
                'image'=> 'file|image|max:5000'
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|max:255',
                'username' => 'required|max:255',
                'email' => 'required|email|max:255',
                'password' => 'required|confirmed',
                'title' => 'required|max:255',
            ]);
        }

        

        //store user

        if (request()->has('image')) {
            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'department' => $request->department,
                'title' => $request->title,
                'image' => $request->image->store('uploads', 'public'),
            ]);
        } else {
            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'department' => $request->department,
                'title' => $request->title,
            ]);
        }


        //sign in
        auth()->attempt($request->only('email', 'password'));

        //redirect
        return redirect()->route('posts');
    }
}
