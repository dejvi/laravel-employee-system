<?php

namespace App\Http\Controllers;

use App\Mail\PostLiked;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Department;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{

    public function __contruct()
    {
        $this->middleware(['auth']);
    }

    public function showDepartment(string $department)
    {
        $users = User::where('department', $department)->sortable()->paginate(10);

        return view('department', [
            'users' => $users,
        ]);
    }

    public function index()
    {
        $users = User::sortable()->paginate(10);

        return view('dashboard', [
            'users' => $users
        ]);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return back();
    }

    public function edit($id)
    {
        $data = User::find($id);
        $departments = Department::all();

        return view('edit_user', [
            'data' => $data,
            'departments' => $departments
        ]);
    }

    public function update(Request $request, $id)
    {
        if (request()->has('image')) {
            $this->validate($request, [
                'name' => 'required|max:255',
                'username' => 'required|max:255',
                'email' => 'required|email|max:255',
                'title' => 'required|max:255',
                'image'=> 'file|image|max:5000'
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required|max:255',
                'username' => 'required|max:255',
                'email' => 'required|email|max:255',
                'title' => 'required|max:255',
            ]);
        }

        if (request()->has('image')) {
            User::find($id)->update([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'department' => $request->department,
                'title' => $request->title,
                'image' => $request->image->store('uploads', 'public'),
            ]);
        } else {
            User::find($id)->update([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'department' => $request->department,
                'title' => $request->title,
            ]);
        }

        return redirect()->route('dashboard');
    }
}
